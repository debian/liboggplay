#include "config.h"

#include <oggplay/oggplay.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static int n_frames = 0;

typedef struct {
  int     last_overrun;
  int     worst_overrun;
  int     overrun_sum;
  int   * histogram;
} overrunInfo;

static overrunInfo *overruns = NULL;

int
dump_streams_callback(OggPlay *player, int num_records, 
                      OggPlayCallbackInfo **info, void *user) {

  int                   i;
  int                   available;
  int                   required;
  int                   overrun;
  OggPlayDataHeader  ** headers;

  if (overruns == NULL) {
    overruns = malloc (sizeof (overrunInfo) * num_records);
    for (i = 0; i < num_records; i++) {
      overruns[i].last_overrun = 0;
      overruns[i].worst_overrun = 0;
      overruns[i].overrun_sum = 0;
      overruns[i].histogram = malloc (sizeof (int));
      overruns[i].histogram[0] = 0;
    }
  }
  
  for (i = 0; i < num_records; i++)
  {

    int j;

    required = oggplay_callback_info_get_required(info[i]);
    available = oggplay_callback_info_get_available(info[i]);
    headers = oggplay_callback_info_get_headers(info[i]);

    overrun = 0;
    for (j = required; j < available; j++) {
      /*OggPlayDataHeader *header;*/

      overrun += oggplay_callback_info_get_record_size(headers[j]);

    }
    
    if (overrun > overruns[i].worst_overrun) {
      int j;
      overruns[i].histogram = realloc (overruns[i].histogram, 
                                        (overrun + 1) * sizeof (int));
      for (j = overruns[i].worst_overrun + 1; j <= overrun; j++) {
        overruns[i].histogram[j] = 0;
      }

      overruns[i].worst_overrun = overrun;
    }

    overruns[i].overrun_sum += overrun;

    overruns[i].histogram[overrun] += 1;

    overruns[i].last_overrun = overrun;
    
    printf("track %d required %d times [", i, required);
    for (j = 0; j < required; j++) {
      if (j > 0) {
        printf(", ");
      }
      printf("%ld", oggplay_callback_info_get_presentation_time(headers[j]));
    }
    printf("] ");
    
  }
  printf("\n");
  
  n_frames += 1;

  return 0;
}

int
main (int argc, char * argv[]) {

  OggPlay       * player;
  OggPlayReader * reader;
  int             i;

  if (argc < 2) {
    printf ("please provide a filename\n");
    exit (1);
  }
  
  reader = oggplay_file_reader_new(argv[1]);

  player = oggplay_open_with_reader(reader);

  if (player == NULL) {
    printf ("could not initialise oggplay with this file\n");
    exit (1);
  }

  printf ("Reading %d tracks ...\n", oggplay_get_num_tracks (player));
  
  for (i = 0; i < oggplay_get_num_tracks (player); i++) {
    printf("\t%s: Track %d ...", oggplay_get_track_typename (player, i), i);
    if (oggplay_get_track_type (player, i) == OGGZ_CONTENT_THEORA) {
      oggplay_set_callback_num_frames (player, i, 1);
    }
    /*
    if (oggplay_get_track_type (player, i) == OGGZ_CONTENT_VORBIS) {
      oggplay_set_offset(player, i, 1000LL);
    }
    */

    if (oggplay_set_track_active(player, i) < 0) {
      printf(" Could not set this track active!\n");
    } else {
      printf(" ok ...\n");
    }
  }

  oggplay_set_data_callback(player, dump_streams_callback, NULL);
  oggplay_start_decoding(player);

  printf("Total %d frames.\n", n_frames);
 
  if (n_frames == 0)
    return 0;

  for (i = 0; i < oggplay_get_num_tracks (player); i++) {
    int j;
    long long mse = 0;
    
    double average = (double)(overruns[i].overrun_sum)/n_frames;
    double hist_bucket_size;
    double cur_bucket;

    if (overruns[i].worst_overrun > 30)
      hist_bucket_size =  overruns[i].worst_overrun/20.0;
    else
      hist_bucket_size = 1.0;
    
    printf("\n%s: Track %d\n", oggplay_get_track_typename (player, i), i);
    printf("\tWorst overrun: %d frames\n", overruns[i].worst_overrun);
    printf("\tAverage overrun: %.3f frames\n", average);
    printf("\tHistogram bucket size: %.3f\n", hist_bucket_size);
    printf("\tHistogram:");

    for 
    (
      cur_bucket = 0; 
      cur_bucket <= overruns[i].worst_overrun; 
      cur_bucket += hist_bucket_size
    ) {
      int sum = 0;
      //printf(" (%f-%f)", cur_bucket, cur_bucket + hist_bucket_size);
      for 
      (
        j = ceil(cur_bucket); 
        j < (cur_bucket + hist_bucket_size) && j <= overruns[i].worst_overrun; 
        j++
      ) {
        sum += overruns[i].histogram[j];
      }
      printf(" %d", sum);
    }

    for (j = 0; j <= overruns[i].worst_overrun; j++) {
      mse += (average - j) * (average - j) * overruns[i].histogram[j];
    }
    printf("\n");
    printf("\tSD of overrun: %f\n", sqrt(mse/n_frames));

            
  }

  return 0;
}
