#include "config.h"

#include <oggplay/oggplay.h>
#include <stdio.h>
#include <stdlib.h>

#include <sndfile.h>

static int n_frames = 0;

void
dump_video_data (OggPlay * player, int track_num, OggPlayVideoData * video_data,
                   int frame) {

  char              fname[256];
  FILE            * f;
  FILE            * g;
  int               i;
  unsigned char   * ptr;
  unsigned char   * ptr2;
  int               y_width;
  int               y_height;
  int               uv_width;
  int               uv_height;

  sprintf(fname, "y_frame%d", frame);
  f = fopen(fname, "w");
  
  oggplay_get_video_y_size(player, track_num, &y_width, &y_height);
  oggplay_get_video_uv_size(player, track_num, &uv_width, &uv_height);
  
  ptr = video_data->y;
  
  for (i = 0; i < y_height; i++) {
    fwrite(ptr, 1, y_width, f);
    ptr += y_width;
  }

  fclose(f);

  sprintf(fname, "u_frame%d", frame);
  f = fopen(fname, "w");
  sprintf(fname, "v_frame%d", frame);
  g = fopen(fname, "w");

  ptr = video_data->u;
  ptr2 = video_data->v;
  
  for (i = 0; i < uv_height; i++) {
    fwrite(ptr, 1, uv_width, f);
    fwrite(ptr2, 1, uv_width, g);
    ptr += uv_width;
    ptr2 += uv_width;
  }

  fclose(f);
  fclose(g);
  
}

static SNDFILE * sndfile = NULL;

void
dump_audio_data (OggPlay * player, int track, OggPlayAudioData * data, 
                int size) {

  if (sndfile == NULL) {
    SF_INFO sfinfo;
    oggplay_get_audio_samplerate(player, track, &(sfinfo.samplerate));
    oggplay_get_audio_channels(player, track, &(sfinfo.channels));
    sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    printf("%d %d %d\n", sfinfo.samplerate, sfinfo.channels, sfinfo.format);

    sndfile = sf_open ("audio", SFM_WRITE, &sfinfo);
  }

  sf_writef_float (sndfile, (float *)data, size);
  
}

int
dump_streams_callback (OggPlay *player, int num_tracks, 
                       OggPlayCallbackInfo **track_info, void *user) {

  int                   i;
  int                   j;
  OggPlayDataHeader  ** headers;
  OggPlayVideoData    * video_data;
  OggPlayAudioData    * audio_data;
  int                   required;
  OggPlayDataType       type;

  for (i = 0; i < num_tracks; i++) {
    type = oggplay_callback_info_get_type(track_info[i]);
    headers = oggplay_callback_info_get_headers(track_info[i]);

    switch (type) {
      case OGGPLAY_INACTIVE:
        break;
      case OGGPLAY_YUV_VIDEO:
        /*
         * there should only be one record
         */
        video_data = oggplay_callback_info_get_video_data(headers[0]);
        dump_video_data(player, i, video_data, n_frames);
        break;
      case OGGPLAY_FLOATS_AUDIO:
        required = oggplay_callback_info_get_required(track_info[i]);
        for (j = 0; j < required; j++) {
          int size;
          size = oggplay_callback_info_get_record_size(headers[j]);
          audio_data = oggplay_callback_info_get_audio_data(headers[j]);
          dump_audio_data(player, i, audio_data, size);
        }
      default:
        break;
    }
  }
  
  n_frames++;

  return 0;
}

int
main (int argc, char * argv[]) {

  OggPlay       * player;
  OggPlayReader * reader;
  int             i;

  if (argc < 2) {
    printf ("please provide a filename\n");
    exit (1);
  }
  
  reader = oggplay_file_reader_new(argv[1]);

  player = oggplay_open_with_reader(reader);

  if (player == NULL) {
    printf ("could not initialise oggplay with this file\n");
    exit (1);
  }

  printf ("there are %d tracks\n", oggplay_get_num_tracks (player));
  
  for (i = 0; i < oggplay_get_num_tracks (player); i++) {
    printf("Track %d is of type %s\n", i, 
                    oggplay_get_track_typename (player, i));
    if (oggplay_get_track_type (player, i) == OGGZ_CONTENT_THEORA) {
      oggplay_set_callback_num_frames (player, i, 1);
    }

    if (oggplay_set_track_active(player, i) < 0) {
      printf("\tNote: Could not set this track active!\n");
    }
  }

  oggplay_set_data_callback(player, dump_streams_callback, NULL);
  oggplay_start_decoding(player);

  sf_close(sndfile);
  
  printf("there were %d frames\n", n_frames);

  return 0;
}
